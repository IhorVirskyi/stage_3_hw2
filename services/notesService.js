const {Note} = require('../models/noteModel');

const getNotes = async ({offset, limit}) => {};

const addNote = async ({text, userId}) => {
  const note = new Note({
    text,
    userId,
  });

  const savedNote = await note.save();

  return savedNote;
};

module.exports = {
  getNotes,
  addNote,
};
