const express = require('express');
const router = new express.Router();

const {registration, signIn} = require('../services/authService');

const {asyncWrapper} = require('../utils/apiUtils');
const {
  registrationValidator,
} = require('../middlewares/validationMidlleware');

router.post(
    '/register',
    registrationValidator,
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      await registration({username, password});

      res.json({message: 'Success'});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      const token = await signIn({username, password});

      res.json({jwt_token: token, message: 'Success'});
    }),
);

module.exports = {
  authRouter: router,
};
