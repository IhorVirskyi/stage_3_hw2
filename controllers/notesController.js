const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../utils/apiUtils');
const {noteValidator} = require('../middlewares/validationMidlleware');
const {addNote} = require('../services/notesService');
const {Note} = require('../models/noteModel');

router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const limit = parseInt(req.query.limit || 10);
      const offset = parseInt(req.query.offset || 0);

      const notes = await Note.find({userId: req.user.userId})
          .skip(offset)
          .limit(limit);
      res.json({message: 'Success', notes});
    }), // / pllus more action contollers
);

router.post(
    '/',
    noteValidator,
    asyncWrapper(async (req, res) => {
      const {text} = req.body;
      await addNote({text, userId: req.user.userId});
      res.json({message: 'Success'});
    }),
);

router.get(
    '/:id',
    asyncWrapper(async (req, res) => {
      const id = req.params.id;

      const note = await Note.findOne({_id: id});
      res.json({message: 'Success', note});
    }),
);

router.put(
    '/:id',
    asyncWrapper(async (req, res) => {
      const id = req.params.id;
      const {text} = req.body;

      const result = await Note.findOneAndUpdate(
          {_id: id},
          {text},
          {
            new: true,
          },
      );
      res.json({message: 'Success', result});
    }),
);

router.patch(
    '/:id',
    asyncWrapper(async (req, res) => {
      const id = req.params.id;

      const note = await Note.findOne({_id: id});
      note.completed = !note.completed;
      await Note.updateOne({_id: id}, note);
      res.json({message: 'Success', note});
    }),
);

router.delete(
    '/:id',
    asyncWrapper(async (req, res) => {
      const id = req.params.id;

      await Note.findOneAndRemove({_id: id});
      res.json({message: 'Success'});
    }),
);

module.exports = {
  notesRouter: router,
};
