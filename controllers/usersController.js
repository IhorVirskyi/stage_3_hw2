const express = require('express');
const router = new express.Router();
const {User} = require('../models/userModel');

const {asyncWrapper} = require('../utils/apiUtils');
const {noteValidator} = require('../middlewares/validationMidlleware');

router.get(
    '/me',
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      const userInfo = await User.findOne({_id: userId});

      const userDto = {
        _id: userInfo._id,
        username: userInfo.username,
        createdAt: userInfo.createdAt,
      };

      res.json({user: userDto});
    }),
);

router.delete(
    '/me',
    noteValidator,
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      await User.findOneAndRemove({_id: userId});
      res.json('Success');
    }),
);

router.patch(
    '/me',
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      const {oldPassword, newPassword} = req.body;

      const user = await User.findOne({_id: userId});

      const hashEquals = await bcrypt.compare(oldPassword, user.passwordHash);

      if (!hashEquals) {
        throw new Error('Invalid old password');
      }

      const newHash = await bcrypt.hash(newPassword, 10);

      await User.updateOne({_id: userId}, {passwordHash: newHash});

      res.json('Success');
    }),
);

module.exports = {
  usersRouter: router,
};
