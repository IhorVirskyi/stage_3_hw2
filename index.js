require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {usersRouter} = require('./controllers/usersController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users', [authMiddleware], usersRouter);
const {NodeCourseError} = require('./utils/errors');

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(
        process.env.DB_HOST,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        },
    );

    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
