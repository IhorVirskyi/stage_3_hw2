const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const noteValidator = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string().min(1),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};
module.exports = {
  registrationValidator,
  noteValidator,
};
